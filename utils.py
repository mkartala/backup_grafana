import urllib3
import json
from datetime import datetime
http = urllib3.PoolManager()
response = ""

def fetch_all_dashboards(token,grafana_url):
    now = datetime.now()
    time = now.strftime("%d/%m/%Y %H:%M:%S")

    url = grafana_url + "/api/search/"

    dashboards_list = (make_get_request(url,token))
    list_with_uids = []
    for dashboard in dashboards_list:
        list_with_uids.append(dashboard["uid"])
    return list_with_uids


def backup_each_dashboard(uid,token,grafana_url,path_to_save_backup):

    now = datetime.now()
    time = now.strftime("%d/%m/%Y %H:%M:%S")
    backup_dir = path_to_save_backup

    dashboard_url = grafana_url + '/api/dashboards/uid/' + uid
    

    dashboard_json = make_get_request(dashboard_url,token)
    meta = dashboard_json['meta']
    slug = meta['slug']

    dashboard = dashboard_json["dashboard"]
    dashboard_path = path_to_save_backup + "/" + slug  + ".json"
    
    try:
        with open(dashboard_path,"w") as f:
            json.dump(dashboard,f)

    except EnvironmentError:
        return "Error with file"
    
    return "Success"

def make_get_request(url, token):
    global response
    """
    This function create a post_request to grafana annotations api
    It needs the body of the request encoded as json, where time and timeEnd  arein milleseconds
    """
    http = urllib3.PoolManager()

    response = http.request("GET", url, headers={"Content-Type": "application/json", "Authorization": token, "Accept": "application/json"},
                            )

    if response.status == 200:
        
        return(json.loads(response.data))

    
    else:
        print("Failed to fetch dashboards id: ")
    