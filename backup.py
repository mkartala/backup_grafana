#!/usr/bin/env python3
import os
import utils
import json

from dotenv import load_dotenv

load_dotenv()
grafana_url = os.getenv("GRAFANA_URL")
token = os.getenv("API_KEY")
path_to_save_backup = os.getenv("BACKUP_FILES_PATH")
utils.fetch_all_dashboards(token,grafana_url)


if __name__ == "__main__":

    for uid in utils.fetch_all_dashboards(token,grafana_url):
        utils.backup_each_dashboard(uid,token,grafana_url,path_to_save_backup)